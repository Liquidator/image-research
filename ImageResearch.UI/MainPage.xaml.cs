﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.System;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ImageResearch.UI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
		private const string NormalBorderBrushName = "NormalBorderBrush";
		private const string SelectedBorderBrushName = "SelectedBorderBrush";
		private Image _selectedImage;

		public MainPage()
        {
            this.InitializeComponent();

			Window.Current.Content.KeyDown += Content_KeyDown;
        }

		private async void Content_KeyDown(object sender, KeyRoutedEventArgs e)
		{
			if (!(IsCtrlKeyPressed() && e.Key == VirtualKey.V))
			{
				return;
			}

			if (_selectedImage == null)
			{
				return;
			}

			var content = Clipboard.GetContent();
			
			if (!content.Contains("Bitmap"))
			{
				return;
			}

			var bitmap = await content.GetBitmapAsync();

			if (bitmap != null)
			{
				var img = new BitmapImage();

				var streamSource = await bitmap.OpenReadAsync();

				img.SetSource(streamSource);

				var buffer = new Windows.Storage.Streams.Buffer((uint)streamSource.Size);

				streamSource.Seek(0);
				
				var a = await streamSource.ReadAsync(buffer, buffer.Capacity, Windows.Storage.Streams.InputStreamOptions.ReadAhead);

				if (_selectedImage == Image)
				{
					_imageBuffer = buffer.ToArray().Skip(54).Take((int)buffer.Length - 54).ToArray();

					var temp = buffer.ToArray();
				}
				else
				{
					_partialImageBuffer = buffer.ToArray().Skip(54).Take((int)buffer.Length - 54).ToArray();
				}

				_selectedImage.Source = img;

				Overlay.Children.Clear();
			}
		}

		private static bool IsCtrlKeyPressed()
		{
			var ctrlState = CoreWindow.GetForCurrentThread().GetKeyState(VirtualKey.Control);
			return (ctrlState & CoreVirtualKeyStates.Down) == CoreVirtualKeyStates.Down;
		}

		byte[] _imageBuffer;
		byte[] _partialImageBuffer;


		int imageHeight => (int)(Image?.ActualHeight ?? 0);
		int imageWidth => (int)(Image?.ActualWidth ?? 0);
		int partialImageHeight => (int)(PartialImage?.ActualHeight ?? 0);
		int partialImageWidth => (int)(PartialImage?.ActualWidth ?? 0);

		Dictionary<(byte A, byte R, byte G, byte B), List<Point>> _partialImageMap = new Dictionary<(byte A, byte R, byte G, byte B), List<Point>>();
		public Point FindImage()
		{
			Overlay.Children.Clear();

			#region Creating partial image map
			_partialImageMap.Clear();

			var points = Enumerable.Range(0, partialImageWidth)
				.SelectMany(i => Enumerable.Range(0, partialImageHeight)
					.Select(j => new Point(i, j))
				).ToList();

			var imagePoints = Enumerable.Range(0, imageWidth)
				.SelectMany(i => Enumerable.Range(0, imageHeight)
					.Select(j => new Point(i, j))
				).ToList();

			foreach (var point in points)
			{
				(byte B, byte G, byte R, byte A) color = GetColor(point, _partialImageBuffer, partialImageWidth, partialImageHeight);

				if (_partialImageMap.Keys.Any(key => AreEqual(color, key)))
				{
					_partialImageMap[_partialImageMap.Keys.First(key => AreEqual(color, key))].Add(point);
				}
				else
				{
					_partialImageMap[color] = new List<Point> { point };
				}
			}

			#endregion Creating partial image map

			#region Checking variants
			var checkPoints = Enumerable.Range(0, imageWidth / partialImageWidth + (imageWidth % partialImageWidth == 0 ? 0 : 1))
				.SelectMany(i => Enumerable.Range(0, imageHeight / partialImageHeight + (imageHeight % partialImageHeight == 0 ? 0 : 1))
					.Select(j => new Point(i * partialImageWidth, j * partialImageHeight))
				).ToList();

			//DrawPoints(checkPoints);

			foreach (var checkPoint in checkPoints)
			{
				//Variants on partial image
				var variants = GetPossibleMatches(GetColor(checkPoint, _imageBuffer, imageWidth, imageHeight));

				variants = GetVariantsWithRarePointsOk(checkPoint, variants);

				if (variants.Any())
				{
					DrawVariants(checkPoint, variants, Colors.Red);
				}
			}
			#endregion Checking variants

			return new Point();
		}

		private List<Point> GetVariantsWithRarePointsOk(Point checkPoint, IEnumerable<Point> variants)
		{
			var result = new List<Point>();

			var rarePoints = _partialImageMap.Keys.OrderBy(key => _partialImageMap[key].Count)
				.SelectMany(key => _partialImageMap[key]).Take(100);

			foreach (var variant in variants)
			{
				var count = .0;

				foreach (var rarePoint in rarePoints)
				{
					var point = new Point(checkPoint.X - variant.X + rarePoint.X, checkPoint.Y - variant.Y + rarePoint.Y);

					if (!IsInsideImage(point))
					{
						count = 0;
						break;
					}

					count += AreEqual(
						GetColor(point, _imageBuffer, imageWidth, imageHeight),
						GetColor(rarePoint, _partialImageBuffer, partialImageWidth, partialImageHeight)) ? 1 : 0;
				}

				if (count / rarePoints.Count() > 0.9)
				{
					result.Add(variant);
				}
			}

			return result;
		}

		private bool IsInsideImage(Point point)
		{
			return point.X >= 0 && point.Y >= 0 && point.X < imageWidth && point.Y < imageHeight;
		}

		private (byte B, byte G, byte R, byte A) GetColor(Point point, byte[] buffer, int width, int height)
		{
			
			int x = ((int)point.X);
			int y = (height - 1 - (int)point.Y);

			///YXZ
			return (
				buffer[(y * width + x) * 4 + 0],
				buffer[(y * width + x) * 4 + 1],
				buffer[(y * width + x) * 4 + 2],
				buffer[(y * width + x) * 4 + 3]
			);
		}

		private void DrawPoints(List<Point> checkPoints)
		{
			foreach (var point in checkPoints)
			{
				var border = new Rectangle
				{
					Height = 1,
					Width = 1,
					Stroke = new SolidColorBrush(Colors.LightGreen),
					StrokeThickness = 1
				};

				border.SetValue(Canvas.TopProperty, point.Y);
				border.SetValue(Canvas.LeftProperty, point.X);

				Overlay.Children.Add(border);
			}
		}

		private void DrawPartialImage(List<Point> points, byte[] buffer, int width, int height, int offset)
		{
			foreach (var point in points)
			{
				var color = GetColor(point, buffer, width, height);

				var border = new Rectangle
				{
					Height = 1,
					Width = 1,
					Stroke = new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B)),
					StrokeThickness = 1
				};

				border.SetValue(Canvas.TopProperty, point.Y);
				border.SetValue(Canvas.LeftProperty, point.X + offset);

				Overlay.Children.Add(border);
			}
		}

		private void DrawVariants(Point checkPoint, List<Point> variants, Color color)
		{
			foreach (var variant in variants)
			{
				var border = new Rectangle {
					Height = PartialImage.ActualHeight,
					Width = PartialImage.ActualWidth,
					Stroke = new SolidColorBrush(color),
					StrokeThickness = 1
				};

				border.SetValue(Canvas.TopProperty, checkPoint.Y - variant.Y);
				border.SetValue(Canvas.LeftProperty, checkPoint.X - variant.X);

				Overlay.Children.Add(border);
			}
		}

		private List<Point> GetPossibleMatches((byte B, byte G, byte R, byte A) color)
		{
			if (_partialImageMap.Keys.Any(key => AreEqual(color, key)))
			{
				return _partialImageMap[_partialImageMap.Keys.First(key => AreEqual(color, key))];
			}
			else
			{
				return new List<Point>();
			}
		}

		private static bool AreEqual((byte B, byte G, byte R, byte A) color, (byte B, byte G, byte R, byte A) key)
		{
			const int ACCURACY = 3;
			return
				Math.Abs(key.A - color.A) < ACCURACY &&
				Math.Abs(key.R - color.R) < ACCURACY &&
				Math.Abs(key.G - color.G) < ACCURACY &&
				Math.Abs(key.B - color.B) < ACCURACY;
		}

		private void Image_PointerEntered(object sender, PointerRoutedEventArgs e)
		{
			if (sender == ImageBorder)
			{
				ImageBorder.BorderBrush = Resources[SelectedBorderBrushName] as Brush;
				PartialImageBorder.BorderBrush = Resources[NormalBorderBrushName] as Brush;

				_selectedImage = Image;
			}
			else if (sender == PartialImageBorder)
			{
				ImageBorder.BorderBrush = Resources[NormalBorderBrushName] as Brush;
				PartialImageBorder.BorderBrush = Resources[SelectedBorderBrushName] as Brush;
				_selectedImage = PartialImage;
			}
			else
			{
				ImageBorder.BorderBrush = Resources[NormalBorderBrushName] as Brush;
				PartialImageBorder.BorderBrush = Resources[NormalBorderBrushName] as Brush;
			}
		}

		private void PartialImage_PointerReleased(object sender, PointerRoutedEventArgs e)
		{

		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var p = FindImage();
		}
	}
}
